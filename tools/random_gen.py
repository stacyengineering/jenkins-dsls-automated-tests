import random
import string


def generate_str(size=6, chars=string.ascii_uppercase + string.digits):
    return "test-{}".format(''.join(random.choice(chars)
                                    for _ in range(size)))
