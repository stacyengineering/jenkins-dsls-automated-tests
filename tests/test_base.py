import json
import os

from unittest import TestCase


class TestBase(TestCase):
    @classmethod
    def setUpClass(cls):
        with open(os.environ.get("BACKEND_CONFIG")) as config:
            conf = json.load(config)
            app_port = conf.get('port_num', "8099")
            app_host = conf.get('host_name', "127.0.0.1")
            cls.app_url = f"http://{app_host}:{app_port}"
