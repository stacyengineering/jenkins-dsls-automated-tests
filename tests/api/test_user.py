import requests

from tests.test_base import TestBase


class TestUser(TestBase):

    def test_get_user(self):
        resp = requests.get(f"{self.app_url}/user")
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.json())

    def test_create_and_get_user(self):
        user_info = {
                "username": "Test",
                "surname": "Userowicz",
                "job": "Verification"
        }
        resp = requests.post(
            f"{self.app_url}/user",
            headers={"Content-type": "application/json"},
            json=user_info)
        self.assertEqual(resp.status_code, 200)
        created_user_info = resp.json()

        self.assertTrue(resp.json())
        self.addCleanup(requests.delete, f"{self.app_url}/user/{created_user_info['id']}")
        get_user_result = list(requests.get(
            f"{self.app_url}/user/{created_user_info['id']}").json().values())[0]
        for key in user_info:
            self.assertEqual(get_user_result[key], user_info[key])

    def test_create_delete_user(self):
        user_info = {
            "username": "Test",
            "surname": "Userowicz",
            "job": "Verification"
        }
        create_resp = requests.post(
            f"{self.app_url}/user",
            headers={"Content-type": "application/json"},
            json=user_info)
        self.assertEqual(create_resp.status_code, 200)
        created_user_info = create_resp.json()
        del_resp = requests.delete(f"{self.app_url}/user/{created_user_info['id']}")
        self.assertEqual(del_resp.status_code, 200)
        get_resp = requests.get(
            f"{self.app_url}/user",
            headers={"Content-type": "application/json"},
            json=user_info)
        self.assertNotIn(created_user_info['id'], get_resp.json())
