from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait


class HomePage(object):
    def __init__(self, driver):
        self.__driver = driver

    def _wait_for_element(self, args):
        WebDriverWait(self.__driver, 10).until(
            expected_conditions.presence_of_element_located(args))

    def _wait_for_element_to_be_clickable(self, args):
        WebDriverWait(self.__driver, 20).until(
            expected_conditions.element_to_be_clickable(args))

    def _generic_id_element(self, object_id):
        self._wait_for_element((By.ID, object_id))
        self._wait_for_element_to_be_clickable((By.ID, object_id))
        return self.__driver.find_element_by_id(object_id)

    @property
    def username_input(self):
        return self._generic_id_element("username-input")

    @property
    def surname_input(self):
        return self._generic_id_element("surname-input")

    @property
    def job_input(self):
        return self._generic_id_element("job-input")

    @property
    def save_button(self):
        return self._generic_id_element("save-button")

    @property
    def user_info_table(self):
        return self._generic_id_element("user-info-table")

    @property
    def user_info_table_rows(self):
        return self.user_info_table.find_elements_by_tag_name("tr")

    def get_user_row_with_attr(self, attr, attr_value):
        attr_map = {"username": 0, "surname": 1, "job": 2}
        for row in self.user_info_table_rows:
            try:
                if row.find_elements_by_tag_name("td")[attr_map[attr]].text == attr_value:
                    return row
            except IndexError:
                pass
