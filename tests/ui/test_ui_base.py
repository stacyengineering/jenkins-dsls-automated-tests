from selenium import webdriver
from tests.test_base import TestBase
from tests.ui.home_page import HomePage


class TestUIBase(TestBase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.driver = webdriver.Chrome("/usr/local/bin/chromedriver")
        cls.driver.get(cls.app_url)
        cls.home_page = HomePage(cls.driver)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()