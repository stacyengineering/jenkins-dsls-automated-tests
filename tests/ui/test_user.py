from time import sleep
import requests

from tests.ui.test_ui_base import TestUIBase
from tools.random_gen import generate_str


class TestUserUI(TestUIBase):
    def setUp(self):
        super().setUp()
        self.username = generate_str()
        self.surname = generate_str()
        self.job = generate_str()


    def test_create_user_and_check_it_exists(self):
        self.home_page.username_input.send_keys(self.username)
        self.home_page.surname_input.send_keys(self.surname)
        self.home_page.job_input.send_keys(self.job)
        self.home_page.save_button.click()
        sleep(3)
        resp = requests.get(f"{self.app_url}/user").json()
        for user in resp:
            user_values = list(user.values())[0]
            if user_values.get("username") == self.username:
                self.addCleanup(requests.delete, f"{self.app_url}/user/{list(user.keys())[0]}")
                self.assertEqual(user_values.get("surname"), self.surname)
                self.assertEqual(user_values.get("job"), self.job)

        self.assertTrue(self.home_page.get_user_row_with_attr("username", self.username))
        self.assertTrue(self.home_page.get_user_row_with_attr("surname", self.surname))
        self.assertTrue(self.home_page.get_user_row_with_attr("job", self.job))
